<?php
return array(
	'class'=>'application.components.UrlManager',
	'urlFormat'=>'path',
	'showScriptName'=>false,
	'caseSensitive'=>false,
	'rules'=>array(

		'' => array('site/index'),
		'<action:restore|ctrl>/key/<key:[\d]+>' => 'site/<action>',
		'confirm/<action:apply>/key/<key:[\d]+>' => 'confirm/<action>',

		'<action:login|forgot|logout|testimonials|signup>' => 'site/<action>',

		/* settings unsubscribe */
		'u/<hash:[a-z0-9]{16}>' => 'settings/notifications/instantunsubscribe',

		/* Short urls */
		'settings' => 'settings/general/index',

        'forum' => 'forum/default/index',
        'forum/<id:\d+>-<slug:.*>' => 'forum/default/view',
	),

);