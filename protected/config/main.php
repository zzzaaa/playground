<?php
$file = (
	(isset($_SERVER['REMOTE_ADDR']) && isset($_SERVER['SERVER_ADDR']) && ($_SERVER['REMOTE_ADDR'] == '127.0.0.1' OR $_SERVER['SERVER_ADDR'] == '::1'))
	|| (isset($_SERVER['LOCAL_MODE']) && $_SERVER['LOCAL_MODE'])
    || (isset($_ENV['DEBUG']))
) ? 'main_dev.php' : 'main_prod.php';

return CMap::mergeArray(
	require(dirname(__FILE__).DIRECTORY_SEPARATOR.$file),
	array(
		'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
		'defaultController' => 'site',
		'sourceLanguage'=>'en',
		'language' => 'en',
		'aliases'=> array(
			'w' => dirname(__FILE__).DIRECTORY_SEPARATOR.'../components/widgets',
		),
		'preload'=>array('log', 'maintenanceMode', 'config'),

		'import'=>array(
			'application.components.*',
			'application.components.behaviors.*',
			'application.components.validators.*',
			'application.components.helpers.*',
			'application.models.*',
			'application.models.forms.*',
		),

		'modules'=>array(
			'settings',
            'forum'
		),

		'behaviors'=>array(
			array(
				'class'=>'application.components.behaviors.BootBehavior',
			),
		),
		'components'=>array(
			'maintenanceMode' => array(
				'class' => 'application.extensions.MaintenanceMode.MaintenanceMode',
				'enabledMode' => true,
				'ips' => [

				],
				'urls'=> [

				],
			),
			'cache'=>array(
				'class'=>'system.caching.CFileCache',
			),
			'session' => array(
				'cookieParams' => array('domain' => '.'.str_replace('www.', '', empty($_SERVER['HTTP_HOST']) ? (empty($_SERVER['SERVER_NAME']) ? '' : $_SERVER['SERVER_NAME']) : $_SERVER['HTTP_HOST'])),
			),
			'urlManager'=>require('urls.php'),
			'log'=>require('log.php'),
			'request' => array(
				'class'=>'HttpRequest',
				'enableCsrfValidation' => true,
				'enableCookieValidation' => true,
				'noCsrfValidationRoutes' => array(
					'site/login',
					'site/index',
					'site/wloginform.submit',
				),
			),
			'authManager'=>array(
				'class' => 'PhpAuthManager',
				'defaultRoles' => array('guest'),
				'showErrors'=>true,
			),
			// проверка SERVER_NAME ради возможности использования этого конфига в cli окружении
			'user'=>array(
				'class'=>'WebUser',
				'stateKeyPrefix'=>!empty($_SERVER['SERVER_NAME']) ? md5(str_replace('www.', '', $_SERVER['SERVER_NAME'])) : '',
				'allowAutoLogin'=>true,
				'defaultReturnUrl'=>'/settings',
				'loginUrl'=>'/login',
				'identityCookie' => array(
					'domain' => !empty($_SERVER['SERVER_NAME']) ? ('.'.str_replace('www.', '', $_SERVER['SERVER_NAME'])) : '',
					'expire'   => 0,
					'path'     => '/',
				),
			),
			'clientScript'=>array(
				'class'=>'ClientScript',
			),
			'assetManager'=>array(
				'excludeFiles' => array('.svn','.gitignore', 'less')
			),
			'errorHandler'=>array(
				'errorAction'=>'site/error',
			),
			'matchMask' => array(
				'class' => 'MatchMask',
				'namedMasks' => [

				],
			),
			'menuMap' => array(
				'class' => 'MenuMap',
			),
			'messages' => array(
				'class'                  => 'DbMessageSource',
				'sourceMessageTable'     => 'lng_source_messages',
				'translatedMessageTable' => 'lng_translated_messages',
				'cachingDuration'        => 3600*24*30,
			),
		),

		'params'=>CMap::mergeArray(
			array(
				'console' => false,
			),
			require('params.php')
		),
	)
);