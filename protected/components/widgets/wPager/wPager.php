<?php
/**
 * вывод постранички в одном из трёх режмов
 *
 * @author  zer0
 * @date    28.04.14
 * @version 1.0
 */

class wPager extends CBasePager
{
	/**
	 * @var string the CSS class for the first page button. Defaults to 'first'.
	 * @since 1.1.11
	 */
	public $firstPageCssClass = 'first';
	/**
	 * @var string the CSS class for the last page button. Defaults to 'last'.
	 * @since 1.1.11
	 */
	public $lastPageCssClass = 'last';
	/**
	 * @var string the CSS class for the internal page buttons. Defaults to 'page'.
	 * @since 1.1.11
	 */
	public $internalPageCssClass = 'page';
	/**
	 * @var string the CSS class for the hidden page buttons. Defaults to 'hidden'.
	 * @since 1.1.11
	 */
	public $hiddenPageCssClass = 'hidden';
	/**
	 * @var string the CSS class for the selected page buttons. Defaults to 'selected'.
	 * @since 1.1.11
	 */
	public $selectedPageCssClass = 'selected';
	/**
	 * @var string the CSS class for the selected page buttons. Defaults to 'selected'.
	 * @since 1.1.11
	 */
	public $linkCssClass = 'ajax-get';
	/**
	 * @var integer maximum number of page buttons that can be displayed. Defaults to 10.
	 */
	public $maxButtonCount = 5;
	public $maxButtonCountSaved;
	/**
	 * @var string the text label for the next page button. Hidden if null.
	 */
	public $nextPageLabel;
	/**
	 * @var string the text label for the previous page button. Hidden if null.
	 */
	public $previousPageLabel;
	/**
	 * @var boolean if last page button shown.
	 */
	public $isLastPage = true;
	/**
	 * @var boolean if first page button shown.
	 */
	public $isFirstPage = true;
	/**
	 * @var string the text label for the first page button. Defaults to '&lt;&lt; First'.
	 */
	public $firstPageLabel;
	/**
	 * @var string the text label for the last page button. Defaults to 'Last &gt;&gt;'.
	 */
	public $lastPageLabel;
	/**
	 * @var array HTML attributes for the pager container tag.
	 */
	public $htmlOptions = array();
	/**
	 * @var string pagination | load-more-link | load-more-scroll | load-more-scroll-floatpager
	 */
	public $mode = 'pagination';
	/**
	 * @var string Контейнер в который будут помещаться полученные с сервера данные.
	 */
	public $container = '#load_more_container';
	/**
	 * @var string - обертка постранички
	 */
	public $floatPaginationContainer = 'float-pagination-wrap';
	/**
	 * @var string - класс айтема
	 */
	public $floatPaginationItemClass = 'float-pagination-item';
	/**
	 * @var boolean - should the URL be changed. Work with "load-more-link" mode only
	 */
	public $isHistoryChanged = false;
	/**
	 * @var array - link HTML options. Work with "load-more-link" mode only
	 */
	public $linkHtmlOptions = array();
	/**
	 * @var string - progress bar selector. If not specified, widget will build its own
	 */
	public $progressBarSelector;
	/**
	 * Callbacks - выполняются в js
	 */
	public $progressBarShowCb;
	public $progressBarHideCb;
	/*
	 * var string - you can specify your own nextPageUrl
	 */
	public $nextPageUrl;
	/**
	 * @var ссылка load more будет содержать вот такой innerHtml. Это свойство имеет выше приоритет чем значение описанное в классе pagiantor.
	 */
	public $loadMoreLabel;

	public $jsInstance;

	public $loadMoreLinkTemplate;

	/**
	 * инициализация
	 */
	public function init()
	{
		if (!isset($this->htmlOptions['id']))
			$this->htmlOptions['id'] = $this->getId();
		if (!isset($this->htmlOptions['class']))
		{
			if (in_array($this->mode, array('pagination', 'load-more-scroll-floatpager')))
				$this->htmlOptions['class'] = 'app-widgets-link-pager';
			else
				$this->htmlOptions['class'] = 'app-widgets-load-more';
		}

		/**
		 * запоминаем переданное значение.
		 * для плавающего слайдера переопределим, чтобы вся постраничка грузилась сразу,
		 * а переданное значение нужно для подсветки нужного кол-ва кнопок
		 */
		$this->maxButtonCountSaved = $this->maxButtonCount;
		if ($this->mode == 'load-more-scroll-floatpager')
			$this->maxButtonCount = 1000;

		if (in_array($this->mode, array('pagination', 'load-more-scroll-floatpager')))
		{
			if ($this->firstPageLabel === null)
				$this->firstPageLabel = '«';
			if ($this->lastPageLabel === null)
				$this->lastPageLabel = '»';
		}

		if (in_array($this->mode, array('load-more-link', 'load-more-scroll', 'load-more-scroll-floatpager')))
		{
			$maxButtonCount = floor($this->maxButtonCountSaved/2);
			if ($maxButtonCount < 1)
				$maxButtonCount = 1;
			$currentPage = $this->getCurrentPage() + 1;

			$obj = array(
				'nextPageUrl' => $this->nextPageUrl ? $this->nextPageUrl : $this->getPages()->getNextPageUrl(),
				'reverse' => $this->getPages()->reverse,
				'container' => $this->container,
				'linkContainerId' => $this->htmlOptions['id'],
				'mode' => $this->mode,
				'block' => $this->getPageCount() > 0 ? ($this->getPageCount() == $currentPage) : true,
				'floatPaginationContainer' => $this->floatPaginationContainer,
				'currentPage' => $currentPage,
				'maxButtonCount' => $maxButtonCount,
				'pageCount' => $this->getPageCount(),
				'floatPaginationItemClass' => $this->floatPaginationItemClass,
				'isHistoryChanged'=>$this->isHistoryChanged ? 1 : 0,
				'progressBarSelector'=>$this->progressBarSelector,
				'progressBarShowCb'=>$this->progressBarShowCb,
				'progressBarHideCb'=>$this->progressBarHideCb,
			);

			if (empty($this->jsInstance)) {
				$registerJs = 'appWidgetLoadMore.init('.CJavaScript::encode($obj).')';
			} else {
				$registerJs = ';'.$this->jsInstance.' = new AppWidgetLoadMore(); '.$this->jsInstance.'.init('.CJavaScript::encode($obj).')';
			}

			Yii::app()->clientScript->registerScript(
				'appWidgetLoadMore_'.uniqid(),
				$registerJs,
				CClientScript::POS_READY
			);
		}
	}

	/**
	 * Executes the widget.
	 * This overrides the parent implementation by displaying the generated page buttons.
	 */
	public function run()
	{
		if (in_array($this->mode, array('pagination', 'load-more-scroll-floatpager'))) {

			$buttonsFloat = $this->createPageButtons();
			if (empty($buttonsFloat))
				return;
			$pagerFloat = CHtml::tag('ul', $this->htmlOptions, implode("\n", $buttonsFloat));

			$this->maxButtonCount = $this->maxButtonCountSaved;
			$buttons = $this->createPageButtons();
			if (empty($buttons))
				return;
			$pager = CHtml::tag('ul', $this->htmlOptions, implode("\n", $buttons));

			echo $pager .
				CHtml::tag('div', array('class' => 'float-pagination-wrap-wrap'),
					CHtml::tag('div', array('class' => "pagination-wrap {$this->floatPaginationContainer}"), $pagerFloat)
				);
		}
		elseif (in_array($this->mode, array('load-more-link'))) {
			$pages = $this->getPages();

			if (
				($pages->getPageCount()==0)
				||($pages->getCurrentPage()+1==$pages->getPageCount() && !$pages->reverse)
				||($pages->getCurrentPage()+1==1 && $pages->reverse)
			) {
				echo '';
			} else {

				$collapsedItems = $pages->getLoadMoreLabel();

				// debug
//				$collapsedItems .= '<br>';
//				$collapsedItems .= 'getItemCount: '.$pages->getItemCount(true);
//				$collapsedItems .= '<br>';
//				$collapsedItems .= 'lastPageSize: '.$pages->lastPageSize;
//				$collapsedItems .= '<br>';
//				$collapsedItems .= 'getFirstPageSize: '.$pages->getFirstPageSize();
//				$collapsedItems .= '<br>';
//				$collapsedItems .= 'getLoadedPages: '.$pages->getLoadedPages();
//				$collapsedItems .= '<br>';
//				$collapsedItems .= 'getPageSize: '.$pages->getPageSize(true);
//				$collapsedItems .= '<br>';
//				$collapsedItems .= 'getPageCount: '.$pages->getPageCount();
//				$collapsedItems .= '<br>';
//				$collapsedItems .= 'getCurrentPage: '.$pages->getCurrentPage();
//				$collapsedItems .= '<br>';

				if ($this->loadMoreLabel === null) {
					$loadMoreLabel = $collapsedItems;
				} else {
					$loadMoreLabel = $this->loadMoreLabel;
				}

				$link = CHtml::link($loadMoreLabel, '#', $this->linkHtmlOptions);

				if ($this->loadMoreLinkTemplate) {
					$tmpl = str_replace("{{link}}", $link, $this->loadMoreLinkTemplate);
					echo CHtml::tag('div', $this->htmlOptions, $tmpl);
				} else {
					echo CHtml::tag('div', $this->htmlOptions, $link);
				}
			}

		} else {
			$pages = $this->getPages();
			if (
				($pages->getPageCount()==0)
				||($pages->getCurrentPage()+1==$pages->getPageCount() && !$pages->reverse)
				||($pages->getCurrentPage()+1==1 && $pages->reverse)
			) {
				echo '';

			} elseif ($this->loadMoreLabel) {
				$link = CHtml::link($this->loadMoreLabel, '#', $this->linkHtmlOptions);

				if ($this->loadMoreLinkTemplate) {
					$tmpl = str_replace("{{link}}", $link, $this->loadMoreLinkTemplate);
					echo CHtml::tag('div', $this->htmlOptions, $tmpl);
				} else {
					echo CHtml::tag('div', $this->htmlOptions, $link);
				}

			} else{
				echo '';
			}
			//echo '';
		}
	}

	/**
	 * Creates the page buttons.
	 * @return array a list of page buttons (in HTML code).
	 */
	protected function createPageButtons()
	{
		if(($pageCount=$this->getPageCount())<=1)
			return array();

		list($beginPage,$endPage)=$this->getPageRange();
		$currentPage=$this->getCurrentPage(false); // currentPage is calculated in getPageRange()
		$buttons=array();

		// first page
		if ($this->isFirstPage)
			$buttons[]=$this->createPageButton($this->firstPageLabel,0,$this->firstPageCssClass,$currentPage<=0,false);

		// previous page
		if ($this->previousPageLabel!== null)
			$buttons[]=$this->createPageButton($this->previousPageLabel,$currentPage-1,$this->firstPageCssClass,$currentPage<=0,false);

		// internal pages
		for($i=$beginPage;$i<=$endPage;++$i)
			$buttons[]=$this->createPageButton($i+1,$i,$this->internalPageCssClass,false,$i==$currentPage);

		// previous page
		if ($this->nextPageLabel!== null)
			$buttons[]=$this->createPageButton($this->nextPageLabel,$currentPage+1,$this->firstPageCssClass,$currentPage>=$pageCount-1,false);

		// last page
		if ($this->isLastPage)
			$buttons[]=$this->createPageButton($this->lastPageLabel,$pageCount-1,$this->lastPageCssClass,$currentPage>=$pageCount-1,false);

		return $buttons;
	}

	/**
	 * Creates a page button.
	 * You may override this method to customize the page buttons.
	 * @param string $label the text label for the button
	 * @param integer $page the page number
	 * @param string $class the CSS class for the page button.
	 * @param boolean $hidden whether this page button is visible
	 * @param boolean $selected whether this page button is selected
	 * @return string the generated button
	 */
	protected function createPageButton($label,$page,$class,$hidden,$selected)
	{
		if($hidden || $selected)
			$class.=' '.($hidden ? $this->hiddenPageCssClass : $this->selectedPageCssClass);
		return '<li class="'.$class.'">'.CHtml::link($label,$this->createPageUrl($page), array('class'=>$this->linkCssClass)).'</li>';
	}

	/**
	 * @return array the begin and end pages that need to be displayed.
	 */
	protected function getPageRange()
	{
		$currentPage=$this->getCurrentPage();
		$pageCount=$this->getPageCount();

		$beginPage=max(0, $currentPage-(int)($this->maxButtonCount/2));
		if(($endPage=$beginPage+$this->maxButtonCount-1)>=$pageCount)
		{
			$endPage=$pageCount-1;
			$beginPage=max(0,$endPage-$this->maxButtonCount+1);
		}
		return array($beginPage,$endPage);
	}
}