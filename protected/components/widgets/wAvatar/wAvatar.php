<?php
class wAvatar extends Widget
{
	public $user;
	public $htmlOptions = array();
	public $is_gray = false;

	public $link;
	public $linkOptions = array();
	public $absoluteUrl = false;

	public $isOnlineShow = true;
	public $isOnlineHint = false;
	public $isOfflineShow = true;

	private $_path;
	private $_alt;

	public function init()
	{
		$this->containerCssClass .= ' app-widgets-avatar';

		// модель юзера
		if ($this->user && !$this->user instanceof ActiveRecord) {
			$this->user = Users::model()->with('usersOnline')->findByPk($this->user);
			if (empty($this->user)) {
				throw new CException('User not found');
			}
		}

		if (!$this->user) {
			$this->isOnlineShow = false;
			$this->isOfflineShow = false;
		}

		// размеры
		if (!empty($this->htmlOptions['width'])) {
			$this->htmlOptions['height'] = $this->htmlOptions['width'];
			switch ($this->htmlOptions['width']) {
				case ($this->htmlOptions['width'] <= 50):
					$size = 'small';
					break;
				case ($this->htmlOptions['width'] <= 100):
					$size = 'medium';
					break;
				case ($this->htmlOptions['width'] > 100):
					$size = 'big';
					break;
			}
		} else {
			$size = 'medium';
			$this->htmlOptions['height'] = $this->htmlOptions['width'] = 100;
		}

		// ссылка
//		if ($this->link === null && $this->user) {
//			$createUrlMethod = $this->absoluteUrl ? 'createAbsoluteUrl' : 'createUrl';
//			$this->link = Yii::app()->$createUrlMethod('/profile/general/index', array('uid' => $this->user->uid_profile));
//		}
//		if (!isset($this->linkOptions['target'])) {
//			$this->linkOptions['target'] = '_blank';
//		}
//		if (!isset($this->linkOptions['data-scrollto'])) {
//			$this->linkOptions['data-scrollto'] = 'top';
//		}

		// путь к картинке
		$abs = $this->absoluteUrl ? Yii::app()->params['site_host'] : '';
		if ($this->user instanceof Users && $this->user->avatar_ext) {
			$this->_path = $abs.'/public/'.$this->user->id.'/avatar/Udimi-buy-solo-'.$size.'-'.$this->user->avatar_ext;
		} else {
			$this->_path = $abs.'/media/img/avatar/Udimi-buy-solo-gray_' . $size . ".jpg";
		}

		// alt
		if (empty($this->htmlOptions['alt'])) {
			$this->_alt = 'Avatar';
		} else {
			$this->_alt = $this->htmlOptions['alt'];
		}

		// css-класс чернобелый режим
		if ($this->is_gray) {
			if (!empty($this->htmlOptions['class'])) {
				$this->htmlOptions['class'] .= ' is_gray';
			} else {
				$this->htmlOptions['class'] = 'is_gray';
			}
		}

		// online/offline
		if ($this->_isOfflineShow()) {
			$this->containerCssClass .= ' offline-show';
		} elseif ($this->_isOnlineShow()) {
			$this->containerCssClass .= ' online-show';
		}

		parent::init(__CLASS__);
	}

	public function run()
	{
		$html  = CHtml::image($this->_path, $this->_alt, $this->htmlOptions);
		if ($this->_isOfflineShow()) {
			$html .= CHtml::tag('div', array('class'=>'was-online-at'), $this->user->getLastSeenAgo(' {val}'));
		} elseif ($this->_isOnlineShow()) {
//			$html .= CHtml::tag('div', array(
//				'class'=>($this->isOnlineHint ? 'live-hover-hint ' : '').$this->_getOnlineIcon(),
//				'data-content'=>'User is online. Last action: '.$this->user->getLastActionAgo('{val}').'.',
//				'data-placement'=>'top',
//				'data-container'=>'body',
//			), '');
		}

//		if ($this->link) {
//			$html = CHtml::link($html, $this->link, $this->linkOptions);
//		}

		return $this->render(array('html'=>$html));
	}

	private function _getOnlineIcon()
	{
		return $this->htmlOptions['width'] < 48 ? 'icon-online-new-small' : 'icon-online-new';
	}

	private function _isOnlineShow()
	{
		return $this->isOnlineShow && $this->user->usersOnline
			&& $this->user->usersOnline->is_online;
	}

	private function _isOfflineShow()
	{
		return $this->isOfflineShow && $this->user->usersOnline
			&& !$this->user->usersOnline->is_online && $this->htmlOptions['width'] >= 90;
	}
}