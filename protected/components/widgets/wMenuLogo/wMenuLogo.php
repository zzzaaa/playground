<?php

class wMenuLogo extends Widget
{

	public function init()
	{
		parent::init(__CLASS__);
	}

	public function run()
	{
		$items = Yii::app()->user->isGuest
			? ['index']
			: ['mypage_logo'];

		$items = MenuItem::loadItems($items, Yii::app()->user->model);
		return $this->render('index', ['items'=>$items]);
	}

	public static function actions()
	{
		Yii::setPathOfAlias(__CLASS__, realpath(dirname(__FILE__)));
		return array(
			'reload'=>array('class'=>'application.components.WidgetBaseAction', 'widget_alias'=>__CLASS__),
		);
	}
} 