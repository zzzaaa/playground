<?php
/* @var $this DefaultController
 * @var $topic Topics
 * @var $comment Comments
 * @var $topicComments[] Comments
 */

$this->beginContent('/layouts/main');
?>
<div class="forum-default-view">

    <div class="b-comment-row">
        <div class="b-col-back">
            <?= CHtml::link('Back', $this->createUrl('index'), ['class' => 'btn ajax-get']); ?>
        </div>
        <div class="b-col-title">
            <h4 class="e-title"><?= $topic->title ?></h4>
        </div>


    </div>

<?php foreach ($topicComments as $com): ?>

    <?= $this->renderPartial('_comment', ['comment' => $com]); ?>

<?php endforeach; ?>

    <div class="b-comment-row">


            <?php
            /** @var $form CActiveForm */
            $form = $this->beginWidget('CActiveForm', ['htmlOptions' => [
                'id' => 'reply', 'data-init' => 'dirty-check', 'class' => 'form-horizontal']
            ]);
            ?>

            <div class="form-group">
                <?= $form->labelEx($comment, 'content', ['class' => 'b-col-label control-label']); ?>
                <div class="b-col-input">
                    <?= $form->textArea($comment, 'content', ['rows' => 3, 'class' => 'form-control', 'placeholder' => 'Yours comment']); ?>
                </div>
            </div>
            <div class="form-group">
                <div class="b-save-buttons">
                    <?= CHtml::submitButton('Create', ['class' => 'ajax-post btn-primary btn']) ?>
                </div>
            </div>
            <?php $this->endWidget(); ?>
        </div>
    </div>
</div>
<?php

$this->endContent();
