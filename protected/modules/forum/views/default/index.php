<?php
/* @var $this DefaultController
 * @var $topics Topics[]
 */

?>

<div class="forum-default-index">

    <div class="e-create">
        <?= CHtml::link('Create', $this->createUrl('create'), ['class' => 'btn btn-primary ajax-get ajax-get-stoppropogation']); ?>
    </div>

    <?php foreach ($topics as $topic):
        //если нет последнего просмотра или он был раньше последнего комменария
        $haveNewReplay = !$topic->lastViews || $topic->lastViews[0]->create_dta < $topic->lastComment[0]->create_dta
            ? ' m-new' : '';
        $link = $this->createUrl('view', ['id' => $topic->id, 'slug' => $topic->slug]);
        ?>
        <div class="b-topic-row">
            <a href="<?= $link ?>" class="big-link">
                <div class="b-col-avatar">
                    <?php $this->widget('w.wAvatar.wAvatar', [
                        'user' => $topic->firstComment[0]->user,
                        'htmlOptions' => [
                            'width' => 80,
                            'class' => 'avatar',
                        ],
                    ]) ?>
                </div>

                <div class="b-col-context">
                    <div class="b-col-stats">
                        <div class="b-stat">
                            <span class="e-stat-val"><?= $topic->views; ?></span>
                            <br>
                            <span class="e-stat-title">views</span>
                        </div>
                        <div class="b-stat <?= $haveNewReplay ?>">
                        <span class="e-stat-val">
                            <?= $haveNewReplay ? $topic->commentsCount . '*' : $topic->commentsCount; ?>
                        </span>
                            <br>
                            <span class="e-stat-title">replays</span>
                        </div>
                    </div>
                    <div class="b-col-comment">
                        <div class="b-title">
                            <?= $topic->title; ?>
                            <span class="e-by">by <?= $topic->firstComment[0]->user->fullname; ?></span>
                            <span class="e-at">at <?= $topic->firstComment[0]->create_dta; ?></span>
                        </div>
                        <div class="b-comment"><?= $topic->firstComment[0]->content; ?></div>
                    </div>

                    <div class="b-col-last">
                        <span class="e-by">last from <?= $topic->lastComment[0]->user->fullname; ?></span>
                    </div>

                </div>
            </a>

        </div>


    <?php endforeach; ?>


</div>


