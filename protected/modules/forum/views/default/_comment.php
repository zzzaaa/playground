<?php
/* @var $this DefaultController
 * @var $comment Comments
 */
?>
<div class="b-comment-row">
    <div class="b-col-avatar">
        <?php $this->widget('w.wAvatar.wAvatar', [
            'user' => $comment->user,
            'htmlOptions' => [
                'width' => '80px',
                'class'=>'avatar',
            ],
        ]) ?>

        <div class="b-stat">
            <span class="e-stat-val"><?=$comment->user->commentsCount?></span>
            <br>
            <span class="e-stat-title">Comments</span>
        </div>
    </div>
    <div class="b-col-comment">
        <h4 class="e-comment-title"><?=$comment->user->fullname;?></h4>
        <div class="e-comment-content"><?=$comment->content;?></div>

        <div class="e-at"><?=$comment->create_dta?>
            <a href="#reply"> Reply</a>
        </div>
    </div>
</div>
