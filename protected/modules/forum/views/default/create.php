<?php
/* @var $this DefaultController
 * @var $topic Topics
 * @var $comment Comments
 */

$this->beginContent('/layouts/main');
?>

<div class="forum-default-create">
    <h3>Create topic</h3>
    <?php
    /** @var $form CActiveForm */
    $form = $this->beginWidget('CActiveForm', ['htmlOptions' => [
        'id' => 'forum-create-topic', 'data-init' => 'dirty-check', 'class' => 'form-horizontal']
    ]);
    ?>

    <div class="form-group">
        <?= $form->labelEx($topic, 'title', ['class' => 'b-col-label control-label']); ?>
        <div class="b-col-input">
            <?= $form->textField($topic, 'title', ['class' => 'form-control', 'placeholder' => 'subject of question']); ?>
        </div>
    </div>


    <div class="form-group">
        <?= $form->labelEx($comment, 'content', ['class' => 'b-col-label control-label']); ?>
        <div class="b-col-input">
            <?= $form->textArea($comment, 'content', ['rows' => 3, 'class' => 'form-control', 'placeholder' => 'Your post']); ?>
        </div>
    </div>
    <div class="form-group">
        <div class="b-save-buttons">
                <?= CHtml::link('Cancel', $this->createUrl('index'), ['class' => 'ajax-get btn']) ?>
                <?= CHtml::submitButton('Create', ['class' => 'ajax-post btn-primary btn']) ?>
        </div>
    </div>
    <?php $this->endWidget(); ?>

</div>

<?php

$this->endContent();
