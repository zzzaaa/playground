<?php

class ForumModule extends WebModule
{
	public function init()
	{
		$this->setImport(array(
			'application.models.forum.*',
			'forum.components.*',
		));
	}

}
