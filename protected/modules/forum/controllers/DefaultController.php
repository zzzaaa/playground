<?php

class DefaultController extends Controller
{
    public function init()
    {
        if (!parent::init()) return false;
        $this->layout = '//layouts/col1center';
        return true;
    }

    public function actionIndex()
    {
        $topics = Topics::model()->getForumDataProvider(Yii::app()->user->id)->getData();
        $this->render('index', ['topics' => $topics]);
    }

    public function actionView($id)
    {

        /** @var Topics $topic */
        $topic = Topics::model()->findByPk($id);

        if (!$topic) {
            throw new CHttpException('404', 'Page not found');
        }

        /** @var CHttpRequest $request */
        $request = Yii::app()->request;

        //если пришел пост запрос на создания комментария
        if ($commentData = $request->getPost(Comments::class)) {
            $newComment = new Comments();
            $newComment->setAttributes($commentData);
            $newComment->user_id = Yii::app()->user->id;
            $newComment->topic_id = $id;

            if ($newComment->validate() && $newComment->save()) {
                $this->jsonResponse([
                    'callback'=>'appMain.showToast("Comment added", "success")',
                    'soft_redirect'=> $this->createUrl('view', ['id' => $newComment->topic_id, 'slug' => $newComment->topic->slug])
                ]);
            }
            else {
                $this->jsonResponse(['error'=>MyUtils::getFirstError($newComment)]);
            }
        }

        $comment = new Comments();
        Topics::incViewCounter($topic->id);

        //обновляем время последнего просмотра
        $lastViewPk = ['user_id' => Yii::app()->user->id, 'topic_id' => $id];

        /** @var TopicLastView $lastView */
        $lastView = TopicLastView::model()->findByPk($lastViewPk);
        if (!$lastView) {
            $lastView = new TopicLastView();
            $lastView->setAttributes($lastViewPk, false);
        }

        $lastView->updateLastView();

        $topicComments = Comments::model()->getCommentsListDataProvider($id)->getData();
        $this->render('view', ['topic' => $topic, 'topicComments' => $topicComments, 'comment' => $comment]);
    }



    public function actionCreate()
    {

        $topic  = new Topics();
        $comment = new Comments();

        /** @var CHttpRequest $request */
        $request = Yii::app()->request;
        $topicData = $request->getPost(Topics::class);
        $commentData = $request->getPost(Comments::class);

        //если пришли пост данные для сохранения
        if ($topicData && $commentData) {
            $topic->setAttributes($topicData);
            $topic->user_id = Yii::app()->user->id;

            $comment->setAttributes($commentData);
            $comment->user_id = Yii::app()->user->id;

            $topic->comments = [$comment];

            if ($topic->withRelated->save(true, ['comments'])){
            $this->jsonResponse([
                'callback'=>'appMain.showToast("Thread created", "success")',
                'soft_redirect'=> $this->createUrl('index')
            ]);

            }
            else {
                $this->appendJsonResponse(['error'=>MyUtils::getFirstError($topic)]);
            }

        }


        $this->render('create', ['topic' => $topic, 'comment' => $comment]);
    }

}