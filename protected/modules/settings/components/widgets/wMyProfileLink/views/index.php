<div class="float js-padding m-padding">
	<?php $form = $this->beginWidget(
		'CActiveForm', array(
		'htmlOptions' => array(
			'class' => 'form-horizontal',
		))) ?>
	<div class="b-row-link">
		<?php if (!Yii::app()->user->model->uid_profile_date): ?>
			<div class="b-link-note">This action is permanent. You won't be able to change it again in the future.</div>
		<?php endif ?>
		<div class="b-col-domain">
			<?= $this->controller->createAbsoluteUrl('/site/index') ?><span class="js-path-static"><?= Yii::app()->user->model->uid_profile ?></span>

			<?php if (!Yii::app()->user->model->uid_profile_date): ?>
				<a href="#" class="e-path-toggler js-path-toggler">change</a>
			<?php endif ?>

		</div>
		<?php if (!Yii::app()->user->model->uid_profile_date): ?>
			<div class="b-col-path js-path-dynam hidden">
				<?= $form->textField(Yii::app()->user->model, 'uid_profile', array('class'=>'form-control')) ?>
			</div>
		<?php endif ?>
		<div class="b-col-button">
			<?php if (!Yii::app()->user->model->uid_profile_date): ?>
				<button type="submit" class="btn btn-primary ajax-post hidden js-link-submit" data-href="<?= $this->controller->createUrl('wmyprofilelink.save')?>" data-widget="wmyprofilelink">Save</button>
			<?php endif ?>
		</div>
	</div>
	<?php $this->endWidget() ?>
</div>
<div class="clearfix"></div>