<?php $this->beginContent('/layouts/main') ?>
<div class="settings-seller-tips">
	<p class="title">Udimi Tips For YOUR Success</p>

	<p class="sub-title">Communicate a Lot</p>
	<ul class="list">
		<li><strong>Always</strong> send “thank you for your order” message to your customer.</li>
		<li>If something is wrong, be sure to contact the buyer with an excuse. Describe the problem and give new and exact deadline.</li>
		<li>Say “<strong>thank you</strong>” to buyers after the order has been completed.</li>
		<li>Contact <strong>past buyers</strong> and send them special offers. Udimi automatically shows a link for you to contact your past customers.</li>
	</ul>

	<p class="sub-title">Be Honest</p>
	<ul class="list">
		<li><strong>Don’t steal from us.</strong> Never try to pull a customer outside of Udimi! Automated system monitors all messaging and you will be <strong>banned for life. There were 738 users banned only this year!</strong> We will not give a warning. This rule is super serious for us!</li>
		<li>Don’t play games with the filter. Automated system monitors activity and traffic can be <strong>reviewed manually</strong> by Udimi team.</li>
	</ul>

	<p class="sub-title">Respect Your List</p>
	<ul class="list">
		<li>Always check buyer’s <strong>swipe and site <u>before</u></strong> you accept the solo. Your subscribers won’t be happy if you send them broken link or a link to junk site.</li>
		<li>Don’t mail your list too often: <strong>two times a week is enough.</strong> Overmailing leads to CTR decrease, spam reports and problems with autoresponder service provider.</li>
	</ul>

	<p class="text-center"><a href="<?= $this->createUrl('seller/index') ?>" class="ajax-get btn btn-primary" data-scrollto="top">Continue</a></p>
</div>
<?php $this->endContent() ?>