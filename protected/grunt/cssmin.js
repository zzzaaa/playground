module.exports = {
	options: {
		shorthandCompacting: false,
		roundingPrecision: -1
	},
	target: {
		files: [{
			expand: true,
			cwd: '../httpdocs/media/css-dev',
			src: ['*.css'],
			dest: '../httpdocs/media/css',
			ext: '.min.css'
		}]
	}
};