<?php
class m170627_120614_create_comment_table extends CDbMigration
{
    const TABLE = 'comments';
    public function safeUp()
    {

        $this->createTable(self::TABLE, [
            'id' => 'pk',
            'topic_id' => 'int',
            'user_id' => 'bigint(20)',
            'content' => 'text',
            'create_dta' => 'datetime DEFAULT NULL',
        ], 'DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci');

        $this->addForeignKey('comment_owner', self::TABLE, 'user_id', 'users', 'id', 'CASCADE');
        $this->addForeignKey('head_topic', self::TABLE, 'topic_id', 'topics', 'id', 'CASCADE');

    }

    public function down()
    {
        $this->dropTable(self::TABLE);
    }
}