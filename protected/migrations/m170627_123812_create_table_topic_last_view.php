<?php
class m170627_123812_create_table_topic_last_view extends CDbMigration
{
    const TABLE = 'topic_last_view';
    public function safeUp()
    {

        $this->createTable(self::TABLE, [
            'topic_id' => 'int',
            'user_id' => 'bigint(20)',
            'create_dta' => 'datetime DEFAULT NULL',
        ]);

        $this->addPrimaryKey('last_view_pk', self::TABLE, 'topic_id, user_id');
        $this->addForeignKey('topic_fk', self::TABLE, 'topic_id', 'topics', 'id', 'CASCADE');
        $this->addForeignKey('user_fk', self::TABLE, 'user_id', 'users', 'id', 'CASCADE');

    }

    public function down()
    {
        $this->dropTable(self::TABLE);
    }
}