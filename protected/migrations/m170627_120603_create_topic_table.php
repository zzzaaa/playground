<?php
class m170627_120603_create_topic_table extends CDbMigration
{
	const TABLE = 'topics';
	public function up()
	{

		$this->createTable(self::TABLE, [
		    'id' => 'pk',
            'user_id' => 'bigint(20)',
            'slug' => 'string',
            'title' => 'string',
            'views' => 'int DEFAULT 0',
            'create_dta' => 'datetime',
            ], 'DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci');

		$this->addForeignKey('topic_owner', self::TABLE, 'user_id', 'users', 'id', 'CASCADE');

	}

	public function down()
	{
	    $this->dropTable(self::TABLE);
	}


}