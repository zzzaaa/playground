<?php

class Users extends ActiveRecord
{
	const ROLE_GUEST = 'guest';
	const ROLE_REAL = 'real';
	const ROLE_VERIFIED = 'verified';
	const ROLE_DELETED = 'deleted';
	const ROLE_POWER = 'power';
	const MONEY_FROZEN_PERIOD_DAYS = 30;

	const SEND_EMAIL_DELAY_SECONDS = 60;
	/**
	 * Границы размера листа
	 */
	const LIST_SIZE_MIN = 1;
	const LIST_SIZE_MAX = 400000;
	/**
	 * Кол-во успешных рейтингов - одно из условий чтобы стать respect seller
	 * Кол-во дней с момента первого успешного solo - одно из условий чтобы стать respect seller
	 */
	const VERIFIED_SELLER_RATINGS = 5;
	const VERIFIED_SELLER_DAYS = 30;
	/**
	 * Количество последних отзывов
	 */
	const LATEST_RATING = 5;

	const BANNED_FOR_COMPLAINT = 'complaint';
	const BANNED_FOR_SIMILAR = 'similar';
	const BANNED_FOR_FREQUENCY = 'frequency';
	const BANNED_FOR_DISPUT = 'disput';
	const BANNED_FOR_SCAM_PAID = 'scam_paid';

	/**
	 * Допустимый процент рефандов юзера
	 */
	const NORMAL_REFUND_RATE = 2;

	const TRIGGER_KENT_ERROR_KENT_LINK = 1;
	const TRIGGER_KENT_ERROR_SELLER_LINK = 2;
	const TRIGGER_KENT_ERROR_NOT_VERIFIED = 3;
	const TRIGGER_KENT_ERROR_NOT_FOUND = 4;

	const AFF_LINK_TYPE_REGULAR = 1;
	const AFF_LINK_TYPE_GMAIL = 2;
	const AFF_LINK_TYPE_RECOMMENDED = 3;
	const AFF_LINK_TYPE_DIRECT = 4;

	/**
	 * Интервал в днях на который скрывается напоминание о past customer
	 * Максимальное кол-во выводимых покупателей
	 */
	const PAST_CUSTOMERS_INTERVAL = 60;
	const PAST_CUSTOMERS_LIMIT = 10;

	const MIN_ABOUTME = 15;

	// для сортировки секций в hidden режиме
	const SH_REJECTED = 1;
	const SH_FUTURE = 2;
	const SH_PAST = 3;
	const SH_RATING = 4;
	const SH_ALIKE = 5;
	const SH_NOTLOGIN = 6;
	const SH_MESSENGER_HIDDEN = 7;
	const SH_PROMOTED = 8;

	const NOT_LOGIN_LAST = 2; //month

	const SH_BTN_HIDDEN = 'Unblock user';
	const SH_BTN_SHOWN = 'Block user';

	/*
	 * interval to calculate sum of last scores
	 */
	const SCORE_LAST_INTERVAL_DAYS = 90;

	const ACCEPT_FAST_HOURS = 3;

	const PRICE_DELTA = 0.05;

	const EXPIRE_LIMITED_RANGE = 30; // берется в расчет X recent solos
	const EXPIRE_RATE_MIN_SOLOS = 10; // предупреждение и бан работают если кол-во соло (всего) больше  X
	const EXPIRE_RATE_WARNING_PERCENT = 10; // процент после которого выводим предупреждение юзеру
	const EXPIRE_RATE_BAN_PERCENT = 30; // реальное значение процента после которого бан seller'а
	const EXPIRE_RATE_BAN_PERCENT_FAKE = 20; // info-процент для юзера, которого он должен бояться

	public $debug;

	public $passwordOld;
	public $passwordNew;
	public $passwordNewConfirm;
	public $passwordForEmail;
	public $facebook_signup, $google_signup;
	public $avatarAllowedExtensions = array('jpg', 'jpeg', 'png', 'gif');
	public $search;
	public $otoExpireDate;
	public $otoIsActive;
	public $count, $count_expired, $percent;
	public $sum;
	public $agree_with_terms;

	public $sn_booked_dta, $sn_rejected_dta, $sn_solo_state;
	public $sh_tp, $sh_alike_fullname, $sh_alike_uid_profile;
	public $last_rate, $last_rate_comment, $last_rate_sales, $last_rate_id, $last_rate_dta, $last_rate_order_clicks;
	public $nearestAvailableDta;

	public $locked_days, $locked_weekdays, $locked_delay, $filter_price, $total_price;

	public $week_order_clicks, $month_order_clicks;
	public $fpToken, $fpToken2;

	public $filter_min_keywords;
	/**
	 * @var class содержит модель кента после выполнения метода triggerKentClick
	 */
	public $kentModel;
	public $sum_balance, $sum_promoters, $sum_frozen, $sum_total;
	public $is_subscribed = false;
	public $is_frauders = false;
	public $seller_amount, $aff_amount, $payment_amount;
	public $signifyd_score;
	public $countGone = 0, $countHardcoded = 0, $countCookied = 0, $countPrime = 0;
	public $count_sales_calculated;
	public $br_seller_total, $br_seller_profit;
	public $signupAvatar;
	public $hlp_seller_amount;
	public $hlp_seller_count;
	public $hlp_buyer_amount;
	public $hlp_buyer_count;
	public $hlp_aff_amount;
	/**
	 * @var string статус верификации Paypal аккаунта
	 */
	protected $_paypalStatus;
	private $_savedSearchFilters;
	private $_buyerSoloStats, $_sellerSoloStats, $_buyerMoneyStats, $_sellerMoneyStats;

	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	public static function roleOptions()
	{
		return array(
			self::ROLE_GUEST => ucfirst(self::ROLE_GUEST),
			self::ROLE_REAL => ucfirst(self::ROLE_REAL),
			self::ROLE_VERIFIED => ucfirst(self::ROLE_VERIFIED),
			self::ROLE_POWER => ucfirst(self::ROLE_POWER),
			self::ROLE_DELETED => ucfirst(self::ROLE_DELETED),
		);
	}

	public static function notAllowedProfileNames()
	{
		return CMap::mergeArray(array_keys(Yii::app()->modules), array(
			'site',
			'oto',
			'facebook',
			'favourites',
			'welcome',
			'tech',
			'filter',
			'my',
			'swipes',
			'activate',
			'prime',
			'assets',
			'framework',
			'protected',
			'media',
			'public',
			'tmp',
			'blog',
			'special',
			'signup',
			'register',
		));
	}

	public function tableName()
	{
		return 'users';
	}

	public function rules()
	{
		return array(
			#array('agree_with_terms', 'compare', 'compareValue' => true, 'message' => 'You must agree to the terms and conditions', 'on'=>'register'),
			array('fullname', 'filter', 'filter' => array($obj = new MyHtmlPurifier(), 'purify'), 'on' => 'register, api-register, control-edit'),
			array('fullname', 'filter', 'filter' => 'MyString::spaceStrip', 'on' => 'register, api-register, control-edit'),
			array('fullname', 'filter', 'filter' => 'trim', 'on' => 'register, api-register, control-edit'),
			array('fullname', 'required', 'on' => 'register, api-register, control-edit', 'message' => 'Name is required'),
			array('fullname', 'length', 'max' => 25, 'on' => 'register, api-register'),
			array('fullname', 'HtmlExist', 'on' => 'register, api-register, control-edit', 'message' => 'Only English letters'),
			array('fullname', 'match', 'pattern' => '/^[a-zA-Z \-’`\']+$/sim', 'message' => 'Only English letters', 'on' => 'register, api-register, control-edit'),
			array('email', 'filter', 'filter' => 'trim', 'on' => 'register, api-register, settingsPrimaryEmail, control-edit'),
			array('email', 'length', 'max' => 255, 'on' => 'register, api-register, settingsPrimaryEmail, control-edit'),
			array('email', 'required', 'on' => 'register, api-register, settingsPrimaryEmail, control-edit', 'message' => 'Email is required'),
			array('email', 'email', 'on' => 'register, api-register, settingsPrimaryEmail, control-edit', 'message' => 'Email invalid'),
			array('email', 'unique', 'on' => 'register, api-register, settingsPrimaryEmail, control-edit', 'message' => 'Email has already been taken'),

			array('signupAvatar', 'safe', 'on' => 'register, control-edit'),
			array('password', 'required', 'on' => 'register, api-register', 'message' => 'Password is required'),
			array('password', 'length', 'min' => 6, 'max' => 255, 'on' => 'register, api-register', 'tooShort' => 'Password is too short'),
			array('paypal_affiliate', 'filter', 'filter' => 'trim', 'on' => 'settingsPaypalAffiliate'),
			array('paypal_affiliate', 'length', 'max' => 255, 'on' => 'settingsPaypalAffiliate'),
			array('paypal_affiliate', 'required', 'on' => 'settingsPaypalAffiliate'),
			array('paypal_affiliate', 'email', 'on' => 'settingsPaypalAffiliate'),
			array('role', 'in', 'range' => array_keys(self::roleOptions()), 'allowEmpty' => false, 'on' => 'control-edit'),
//			array('notes', 'safe', 'on'=>'control-edit'),
			array('is_banned_buyer', 'boolean', 'on' => 'control-edit'),
			array('paypal_email', 'length', 'max' => 255, 'on' => 'settingsPaypalEmail'),
			array('paypal_email', 'required', 'on' => 'settingsPaypalEmail'),
			array('paypal_email', 'email', 'on' => 'settingsPaypalEmail'),
			array('uid_profile', 'filter', 'filter' => 'trim', 'on' => 'settingsProfileLink'),
			array('uid_profile', 'required', 'on' => 'settingsProfileLink'),
			array('uid_profile_date', 'compare', 'compareValue' => '1', 'allowEmpty' => true, 'message' => 'You are not allowed to change the name again', 'on' => 'settingsProfileLink'),
			array('uid_profile', 'length', 'min' => 5, 'max' => 32, 'on' => 'settingsProfileLink'),
			array('uid_profile', 'match', 'pattern' => '/^[a-z0-9_]{5,32}+$/', 'message' => 'Invalid format. Please use latin letters, numbers or "_"', 'on' => 'settingsProfileLink'),
			array('uid_profile', 'in', 'range' => self::notAllowedProfileNames(), 'not' => true, 'allowEmpty' => false, 'on' => 'settingsProfileLink', 'message' => 'This address is already taken'),
			array('uid_profile', 'unique', 'message' => 'This address is already taken', 'on' => 'settingsProfileLink'),
			array('passwordNew, passwordNewConfirm', 'length', 'min' => 6, 'max' => 24, 'on' => 'settingsChangePassword, restore'),
			array('passwordNew, passwordNewConfirm', 'required', 'on' => 'settingsChangePassword, restore', 'message' => '{attribute} is required'),
			array('passwordOld', 'required', 'on' => 'settingsChangePassword'),
			array('passwordNew', 'compare', 'compareAttribute' => 'passwordNewConfirm', 'on' => 'settingsChangePassword, restore', 'message' => 'New passwords do not match'),
			array('passwordOld', 'validateOldPassword', 'on' => 'settingsChangePassword'),
			array('isSoundNewMessage', 'in', 'range' => array(0, 1), 'allowEmpty' => true, 'on' => 'settingsSound'),
			array('isSoundNewProposal', 'in', 'range' => array(0, 1), 'allowEmpty' => true, 'on' => 'settingsSound'),
			array('is_list_verify', 'required', 'on' => 'settingsListSize'),
			array('is_list_verify', 'in', 'range' => array(0, 1), 'on' => 'settingsListSize'),
			array('list_size_unverify', 'required', 'on' => 'settingsListSize'),
			array('list_size_unverify', 'length', 'max' => 6, 'on' => 'settingsListSize'),
			array('list_size_unverify', 'numerical', 'integerOnly' => true, 'on' => 'settingsListSize'),
			array('list_size_unverify', 'compare', 'compareValue' => self::LIST_SIZE_MIN, 'operator' => '>=', 'message' => 'Your unverified list size is zero.', 'on' => 'settingsListSize'),
			array('list_size_unverify', 'compare', 'compareValue' => self::LIST_SIZE_MAX, 'operator' => '<=', 'on' => 'settingsListSize'),
			array('avatar_ext', 'in', 'range' => $this->avatarAllowedExtensions, 'allowEmpty' => true, 'on' => 'mypageUploadAvatar'),
			array('aboutme', 'filter', 'filter' => 'trim', 'on' => 'aboutme, aboutme-pe'),
			array('aboutme', 'filter', 'filter' => 'MyString::nlStrip', 'on' => 'aboutme, aboutme-pe'),
			array('aboutme', 'filter', 'filter' => array($obj = new MyHtmlPurifier(), 'purify'), 'on' => 'aboutme, aboutme-pe'),
			// edit inline on mypage
			array('aboutme', 'required', 'on' => 'aboutme', 'message' => 'Please write something about yourself'),
			array('aboutme', 'length', 'max' => 150, 'min' => self::MIN_ABOUTME, 'on' => 'aboutme'),
			array('aboutme', 'HtmlExist', 'on' => 'aboutme'),
			array('aboutme', 'LinksExist', 'on' => 'aboutme', 'message' => 'Links are not allowed here'),
			// edit on separate page 'Edit my profile'
			array('aboutme', 'required', 'on' => 'aboutme-pe', 'message' => '"Text to show in search results" is required'),
			array(
				'aboutme',
				'length',
				'max' => 150,
				'min' => self::MIN_ABOUTME,
				'on' => 'aboutme-pe',
				'tooLong' => '"Text to show in search results" is too long (maximum is 150 characters)',
				'tooShort' => '"Text to show in search results" is too short (minimum is 15 characters)'
			),
			array('aboutme', 'HtmlExist', 'on' => 'aboutme-pe', 'message' => '"Text to show in search results" should not contain HTML tags'),
			array('aboutme', 'LinksExist', 'on' => 'aboutme-pe', 'message' => 'Links are not allowed in "Text to show in search results"'),
			array('fpToken', 'numerical', 'integerOnly' => true),
			array('fpToken2', 'length', 'max' => 32),
			array('search', 'length', 'max' => 80, 'on' => 'search'),
			array('filter_min_keywords', 'numerical', 'integerOnly' => true, 'on' => 'search-keywords-users'),
			// reply PM
			array('reply_pm_stop_words', 'filter', 'filter' => 'trim', 'on' => 'settingsReplyPm'),
			array('reply_pm_stop_words', 'filter', 'filter' => array($obj = new MyHtmlPurifier(), 'purify'), 'on' => 'settingsReplyPm'),
			array('reply_pm_stop_words', 'length', 'allowEmpty' => true, 'max' => 150, 'min' => 10, 'on' => 'settingsReplyPm'),
			array('reply_pm_stop_words', 'HtmlExist', 'on' => 'settingsReplyPm'),
			array('reply_pm_stop_words', 'LinksExist', 'on' => 'settingsReplyPm', 'message' => 'Links are not allowed here'),
			// lng
			array('lang', 'in', 'range' => array_keys(Yii::app()->params['translatedLanguages']), 'allowEmpty' => false, 'on' => 'settingsLng'),
		);
	}

	public function validateOldPassword($attribute, $params)
	{
		if (!CPasswordHelper::verifyPassword($this->$attribute, $this->password)) {
			$this->addError('password', 'Wrong old password');
		}
	}

	public function relations()
	{
		return array(
			'profileSolos' => array(self::HAS_ONE, 'ProfileSolo', 'id_user'),
			'profileBuyer' => array(self::HAS_ONE, 'ProfileBuyer', 'id_user'),
			'nicheUsers' => array(self::HAS_MANY, 'NicheUsers', 'id_user'),
			'niche' => array(self::HAS_MANY, 'Niches', array('id_niche' => 'id'), 'through' => 'nicheUsers'),
			'userIdentity' => array(self::HAS_MANY, 'UserIdentity', 'id_user'),
			'swipes' => array(self::HAS_MANY, 'Swipes', 'id_user'),
			'solosSeller' => array(self::HAS_MANY, 'Solos', 'id_seller'),
			'solosBuyer' => array(self::HAS_MANY, 'Solos', 'id_buyer'),
			'solosKent' => array(self::HAS_MANY, 'Solos', 'id_kent'),
			'autoresponderUsers' => array(self::HAS_MANY, 'AutoresponderUsers', 'id_user'),
			'forumProfile' => array(self::HAS_ONE, 'ForumProfile', 'id_user'),
			'usersFunds' => array(self::HAS_ONE, 'UsersFunds', 'id_user'),
			'banWithdrawal' => array(self::HAS_ONE, 'BanWithdrawal', 'id_user'),
			'referral' => array(self::BELONGS_TO, 'Users', 'id_ref'),
			'awardsUsers' => array(self::HAS_MANY, 'AwardsUsers', 'id_user'),
			'promoteFunds' => array(self::HAS_MANY, 'PromoteFunds', 'id_seller'),
			'pmHistory' => array(self::HAS_MANY, 'PmHistory', 'id_user'),
			'pmDialogsPartner' => array(self::HAS_ONE, 'PmDialogs', 'id_partner'),
			'idFunds' => array(self::HAS_MANY, 'Funds', 'id_user'),
			'insPaypal' => array(self::HAS_MANY, 'InsPaypal', 'custom'),
			'insStripe' => array(self::HAS_MANY, 'InsStripe', 'id_user'),
			'forumPosts' => array(self::HAS_MANY, 'forumPosts', 'id_user'),
			'searchBookingDates' => array(self::HAS_MANY, 'SearchBookingDates', 'id_user'),
			'idSubscription' => array(self::HAS_ONE, 'Subscriptions', 'id_user'),
			'idSubscriptionActive' => array(self::HAS_ONE, 'Subscriptions', 'id_user', 'on' => 'idSubscriptionActive.is_active=1'),
			'affiliatesBuyer' => array(self::HAS_MANY, 'Affiliates', 'id_buyer'),
			'usersOnline' => array(self::HAS_ONE, 'UsersOnline', 'id_user'),
			'usersSummary' => array(self::HAS_ONE, 'UsersSummary', 'id_user'),
			'affiliatesCustomize' => array(self::HAS_ONE, 'AffiliatesCustomize', 'id_user'),
			'idPaymentRequest' => array(self::HAS_MANY, 'PaymentRequests', 'id_user'),
			'idRatings' => [self::HAS_MANY, 'Ratings', 'id_user'],
			'pushProfiles' => [self::HAS_MANY, 'PushProfile', 'id_user'],
			'idFavourite' => [self::HAS_MANY, 'Favourites', 'id_favourite'],
			'idRecentView' => [self::HAS_MANY, 'RecentlyView', 'id_user_view'],
			'blocked' => array(self::HAS_MANY, 'PmBlock', 'id_block_user'),
			'automessageRead' => array(self::HAS_ONE, 'HlpAutomessageRead', 'id_user'),
			'commentsCount' => array(self::STAT, 'Comments', 'user_id'),
		);
	}

	public function attributeLabels()
	{
		return array(
			'email' => 'Email',
			'password' => 'Password',
			'fullname' => 'Name',
			'passwordOld' => 'Old password',
			'passwordNew' => 'New password',
			'passwordNewConfirm' => 'Retype password',
			'role' => 'Role',
			'paypal_email' => 'PayPal email',
			'paypal_affiliate' => 'PayPal email',
			'list_size_unverify' => 'Unverified list size',
			'agree_with_terms' => 'I agree with site <a target="_blank" href="' . Yii::app()->createUrl('/site/legal') . '">terms & conditions</a>',
			'isSoundNewMessage' => 'Play sound on new messages',
			'isSoundNewProposal' => 'Play sound on new solo orders',
			'uid_profile' => 'Profile link',
			'aboutme' => 'About yourself',
			'is_banned_buyer' => 'Ban buyer',
			'silence_reason' => 'Ban reason',
			'reply_pm_stop_words' => 'Email end key phrases',
		);
	}

	public function validatePassword($password)
	{
		return CPasswordHelper::verifyPassword($password, $this->password);
	}

	public function hashPassword($password)
	{
		return CPasswordHelper::hashPassword($password);
	}

	public function makeUser()
	{
		// niche (HAS_MANY)
		$modelNicheUsersArray = array();
		$nichesModel = Niches::model()->findAllByAttributes(array('default' => 1));
		if (!empty($nichesModel)) {
			foreach ($nichesModel as $item) {
				$modelNicheUsers = new NicheUsers;
				$modelNicheUsers->id_niche = $item->id;
				$modelNicheUsersArray[] = $modelNicheUsers;
			}
		}
		$this->nicheUsers = $modelNicheUsersArray;

		// seller (HAS_ONE)
		$this->profileSolos = new ProfileSolo;

		// seller (HAS_ONE)
		$this->profileBuyer = new ProfileBuyer;
		$this->profileBuyer->generateBrokerKey();

		// users_online (HAS_ONE)
		$this->usersOnline = new UsersOnline;

		$this->withRelated->save(true, array('profileSolos', 'profileBuyer', 'nicheUsers', 'usersOnline'));

		// autoconfirm
		$this->is_confirmed = 1;

		$this->update(['is_confirmed']);
	}

	public function getUserConfig()
	{
		$uc = new DUserConfig();
		$uc->id_user = $this->id;

		return $uc;
	}

	public function getLanguage()
	{
		if (!in_array($this->lang, array_keys(Yii::app()->params['translatedLanguages']))) {
			return Yii::app()->params['defaultLanguage'];
		} else {
			return $this->lang;
		}
	}

	protected function afterFind()
	{
		if (!empty($this->fullname)) {
			$this->fullname = mb_convert_case($this->fullname, MB_CASE_TITLE, "UTF-8");
		}

		parent::afterFind();
	}

	protected function beforeSave()
	{
		if (parent::beforeSave()) {

			if ($this->scenario == 'register' || $this->scenario == 'api-register') {
				$this->passwordForEmail = $this->password;
				$this->password = $this->hashPassword($this->password);
				$this->role = self::ROLE_GUEST;
				$this->uid = MyUtils::generateUniqueStringUid(5, 'users', 'uid');
				$this->uid_profile = MyUtils::generateUniqueStringUid(5, 'users', 'uid_profile');
				$this->dta_reg = date('Y-m-d H:i:s');
				$this->list_size_unverify = $this->list_size = 1;
				$this->accept_in = ProfileSolo::ORDER_TTL_OLDEST * 60 * 60;
				$this->hash = MyUtils::generateUniqueStringUid(16, 'users', 'hash');
			}
			if (in_array($this->scenario, array('settingsChangePassword', 'restore'))) {
				$this->password = $this->hashPassword($this->passwordNew);
			}
			if ($this->scenario == 'settingsProfileLink') {
				$this->uid_profile_date = date('Y-m-d H:i:s');
			}

			return true;
		} else {
			return false;
		}
	}

	protected function afterSave()
	{
		parent::afterSave();

		if ($this->role == self::ROLE_DELETED) {
			UserIdentityKeys::remove($this, 'cookie_login');
		}
	}
}