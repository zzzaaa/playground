<?php

/**
 * @property integer $id
 * @property integer $user_id
 * @property string $slug
 * @property string $title
 * @property integer $views
 * @property integer $commentsCount
 * @property string $create_dta
 *
 * The followings are the available model relations:
 * @property Comments[] $comments
 * @property Users $user
 * @property Comments[] $lastComment
 * @property Comments[] $firstComment
 * @property TopicLastView[] $lastViews
 */
class Topics extends ActiveRecord
{
    public function behaviors()
    {
        return array(
            'CTimestampBehavior' => array(
                'class' => 'zii.behaviors.CTimestampBehavior',
                'createAttribute' => 'create_dta',
                'updateAttribute' => 'create_dta',
            ),
            'withRelated' => array(
                'class' => 'ext.wr.WithRelatedBehavior',
            ),
        );
    }

    public function tableName()
    {
        return 'topics';
    }

    public function rules()
    {
        return array(
            array('title', 'required', 'on' => 'insert'),
            array('title', 'filter', 'filter' => 'MyUtils::convert2db', 'on' => 'insert'),
            array('slug, title', 'length', 'max' => 255),
        );
    }

    public function relations()
    {
        return array(
            'comments' => array(self::HAS_MANY, 'Comments', 'topic_id'),
            'user' => array(self::BELONGS_TO, 'Users', 'user_id'),
            'commentsCount' => array(self::STAT, 'Comments', 'topic_id'),
            'lastViews' => array(self::HAS_MANY, 'TopicLastView', 'topic_id'),
            'firstComment' => array(self::HAS_MANY, 'Comments', 'topic_id', 'order' => 'firstComment.id ASC', 'limit' => 1),
            'lastComment' => array(self::HAS_MANY, 'Comments', 'topic_id', 'order' => 'lastComment.id DESC', 'limit' => 1),
        );
    }

    public function setAttribute($name, $value)
    {
        switch ($name) {
            case 'title':
                $slug = MyUtils::transliterate($value);
                $slug = preg_replace('/\s+/i', '-', $slug);
                $slug = preg_replace('/[^\w\d\-]+/i', '', $slug);
                $this->slug = $slug;
                break;
        }

        return parent::setAttribute($name, $value);
    }

    public function attributeLabels()
    {
        return array(
            'title' => 'Subject',
        );
    }

    /**
     * @param int|null $lastViewForUserId
     * @return CActiveDataProvider
     */
    public function getForumDataProvider($lastViewForUserId = null)
    {
        $criteria = new CDbCriteria();
        $criteria->with = [
            'firstComment',
            'firstComment.user' => ['alias' => 'frtUser'],
            'lastComment',
            'lastComment.user' => ['alias' => 'lstUser']
        ];

        if ($lastViewForUserId) {
            $criteria->with[] = [
                'lastViews' => [
                    'scopes' => [
                        'byUser' => $lastViewForUserId
                    ]
                ]
            ];
        }

        return new CActiveDataProvider($this, [
            'criteria' => $criteria,
            'pagination' => false
        ]);

    }


    /**
     * @return Topics the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @param int $id
     */
    public static function incViewCounter($id)
    {
        /** @var $db CDbConnection */
        $db = Yii::app()->db;
        $db->createCommand('UPDATE topics SET views=views+1 WHERE id = :id')->execute([':id' => (int)$id]);
    }


}
