<?php

/**
 * @property integer $id
 * @property integer $topic_id
 * @property integer $user_id
 * @property string $content
 * @property string $create_dta
 *
 * The followings are the available model relations:
 * @property Users $user
 * @property Topics $topic
 */
class Comments extends ActiveRecord
{
    public function behaviors()
    {
        return array(
            'CTimestampBehavior' => array(
                'class' => 'zii.behaviors.CTimestampBehavior',
                'createAttribute' => 'create_dta',
                'updateAttribute' => 'create_dta',
            )
        );
    }

    public function tableName()
    {
        return 'comments';
    }

    public function rules()
    {
        return array(
            array('content', 'required', 'on' => 'insert'),
            array('content', 'filter', 'filter' => 'MyUtils::convert2db', 'on' => 'insert'),
            array('content', 'length', 'min' => 1),
        );
    }

    public function relations()
    {
        return array(
            'user' => array(self::BELONGS_TO, 'Users', 'user_id'),
            'topic' => array(self::BELONGS_TO, 'Topics', 'topic_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'content' => 'Comment',
        );
    }

    /**
     * @return Comments the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }


    /**
     * @param int $topicId
     */
    public function getCommentsListDataProvider($topicId)
    {
        $criteria = new CDbCriteria();
        $criteria->alias = 'com';
        $criteria->addColumnCondition(['topic_id' => $topicId]);
        $criteria->order = ' com.id ASC';
        $criteria->with = ['user', 'user.commentsCount'];

        return new CActiveDataProvider($this, ['criteria' => $criteria, 'pagination' => false]);
    }
}
