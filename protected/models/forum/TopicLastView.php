<?php

/**
 * @property integer $topic_id
 * @property string $user_id
 * @property string $create_dta
 */
class TopicLastView extends CActiveRecord
{
	public function tableName()
	{
		return 'topic_last_view';
	}

    public function primaryKey()
    {
       return ['topic_id', 'user_id'];
    }

    public function behaviors(){
        return array(
            'CTimestampBehavior' => array(
                'class' => 'zii.behaviors.CTimestampBehavior',
                'createAttribute' => 'create_dta',
                'updateAttribute' => 'create_dta',
            )
        );
    }

    public function updateLastView()
    {
        $this->create_dta = time();
        return $this->save();
    }

	public function byUser($userId)
    {
        $criteria = new CDbCriteria();
        $criteria->alias = 'tlv';
        $criteria->addColumnCondition(['tlv.user_id' => $userId]);
        $this->getDbCriteria()->mergeWith($criteria);

        return $this;
    }

	/**
	 * @return TopicLastView the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
